//functions
const add = (a: number, b:number): number => {
    return a + b
}

const logMessage = (message : any): void => {
    console.log(message)
}

logMessage("Helllo!")
logMessage(add(2,3))


let subtract = function (c: number, d:number): number {
    return c-d
}

//Type Defination
// type mathFunctiom = (a: number, b:number) => number

interface mathFunctiom{
    (a: number, b:number) : number
} 

let multiply: mathFunctiom = function(c,d){
    return c*d;
}

logMessage(multiply(2,2));

//Optional parameters

const addAll = (a: number, b: number, c?:number):
number => {

    if(typeof c !== 'undefined'){

        return a + b + c
    }
    return a + b
}

const sumAll = (a: number, b: number, c:number = 2):
number => {
        return a + b + c
}

logMessage(addAll(2,3,2))
logMessage(addAll(2,3))
logMessage(sumAll(2,3))
// logMessage(sumAll(undefined,3)) --> ERROR ONLY FOR ME


//Rest parameter
const total = ( a: number,...nums: number[]): number => {

    return a + nums.reduce((previous,current) => previous + current);
}

logMessage(total(10,2,3))


//NEVER Type
const createError = (errMsg : string): never => {
    throw new Error(errMsg)
}

const infinite = () => {
    let i: number = 1
    while(true){
        i++
        if(i > 100) break
    }
}

// custom type gaurd
const isNumber = (value:any): boolean => {
    return typeof value === 'number' ? true : false
}

// use of never type
const numberOrString = (value: number | string):
string => {
    if(typeof value === "string") return 'string'
    if(isNumber(value)) return 'number'

    return createError('This should never happen!')
}