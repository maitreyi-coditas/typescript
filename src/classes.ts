// CLASSES

// class Coder {

//     name:string
//     music:string
//     age: number
//     lang: string

//     constructor(
//         name:string,
//         music:string,
//         age: number,
//         lang: string
//         ){
//         this.name = name
//         this.music = music
//         this.age = age
//         this.lang = lang
//     }
 
// }

class Coder {

    secondLang! : string

    constructor(
        public readonly name:string,
        public music:string,
        private age: number,
        protected lang: string = "Typescript"
        ){
        this.name = name
        this.music = music
        this.age = age
        this.lang = lang
    }
 
    public getAge(){
        return `Hello I'm ${this.age}`
    }
    
}


// const dave = new Coder('Dave','Rock',42,'Typescript')
const dave = new Coder('Dave','Rock',42)
console.log(dave.getAge())
// console.log(dave.age)
// console.log(dave.lang)


class WebDev extends Coder{

    constructor(
        public computer : string,
        name: string,
        music: string,
        age: number,
        
    ){
        super(name, music, age)
        this.computer = computer
    }


    public getLang(){
        return `I code in ${this.lang}`
    }
}

const sara = new WebDev("Mac","sara","lofi",25);
console.log(sara.getLang())
console.log(sara.getAge())
// console.log(sara.age)
// console.log(sara.lang)


// INTERFACES

interface Musician {

    userName : string,
    instrument : string,
    play(action : string) : string
}

class Guitarist implements Musician{

    userName : string;
    instrument: string;
    
    constructor(userName:string, instrument:string) {

        this.userName = userName;
        this.instrument = instrument;

    }
  
    play(action : string){

        return `${this.userName} ${action} the ${this.instrument}`
    }
}

const page = new Guitarist("Jimmy",'guitar')
console.log(page.play('strums'))



class Peeps{

    static count : number = 0 

    static getCount(): number {

        return Peeps.count
    }

    public id: number
     
    constructor(public name : string){

        this.name = name
        this.id = ++Peeps.count
    }
}

const john = new Peeps("John");
const steve = new Peeps("Steve");
const  amy= new Peeps("Amy");

console.log(Peeps.count) //3

console.log(steve.id)
console.log(john.id)
console.log(amy.id)



// GETTERS AND SETTERS

class Bands {

    private dataState : string[]

    constructor(){
        this.dataState = []
    }

    public get data(): string[] {
        return this.dataState
    }

    public set data(value : string[]){

        if(Array.isArray(value) && value.every((element)=> typeof element === "string")){
            this.dataState = value
            return  
        } else throw new Error("Parameter is not an array of strings")
    }
}

const myBand = new Bands()

myBand.data = ['Neil Young','Led Zep']
console.log(myBand.data)

myBand.data = [...myBand.data, "ZZ Top"]
console.log(myBand.data)

myBand.data = ['Van Halen']