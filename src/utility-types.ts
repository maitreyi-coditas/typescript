//Utiliy Types 


interface Assignment {

    studentId : string,
    title : string,
    grade : number,
    verified?: boolean,

}

const updateAssignment = (assign : Assignment, propsToUpdate: Partial<Assignment>):
Assignment => {
    return {...assign, ...propsToUpdate}
}

const assign1 : Assignment = {
    
    studentId : "CompScience",
    title:"Final Project",
    grade: 0,
}


console.log(updateAssignment(assign1, {grade : 95}))
const assignGraded: Assignment = updateAssignment(assign1, {grade: 95})


// Required and Readonly

const recordAssignment = (assign : Required<Assignment>): Assignment => {
    return assign
}

const assignVerified : Readonly<Assignment> = {
    ...assignGraded, verified : true
}

recordAssignment({...assignGraded, verified:true})


// Record


const hexColorMap : Record<string,string> = {

    red: "FF0000",

    green : "00FF00",

    blue : "0000FF",

}

type Students = "Sara" | "Kelly"

type LetterGrades = "A" | "B" | "C" | "D"

const finalGrades : Record<Students, LetterGrades> ={
    Sara : "B",
    Kelly : "C"
}


interface Grades {

    assign1 : number,
    assign2 : number,
}

const gradeData : Record<Students, Grades> = {
    Sara : { assign1 : 85, assign2 : 93},
    Kelly : { assign1 : 76, assign2 :15}
}


//Pick and Omit

type AssignResult = Pick<Assignment, "studentId" | "grade">

const score : AssignResult = {
    studentId : "A123",
    grade: 85
}


type AssignPreview = Omit<Assignment, "grade" | "verified">

const preview : AssignPreview = {
    studentId : "A123",
    title : "Final Project"
}


//Exclude and extract

type adjustedGrade = Exclude<LetterGrades, "D">

type highGrades = Extract<LetterGrades, "A" | "B">

//Non nullable

type AllPossibleGrades = "Dave" | "John" | "null" | undefined

type NameOnly = NonNullable<AllPossibleGrades>


// Return Type

type newAssign = {title : string, points : number}

const createNewAssign = (title : string, points : number): newAssign => {
    return {title, points}

}

type NewAssign = ReturnType<typeof createNewAssign>


const tsAssign : NewAssign = createNewAssign("Utility Types", 100)

console.log(tsAssign);



// Para,eters


type AssignParams = Parameters<typeof createNewAssign>

const assignArgs : AssignParams = ["Generics",100]

const tsAssign2 : NewAssign = createNewAssign(...assignArgs)

console.log(tsAssign2)


//Awaited - helps with the returntype of a promise

interface User {

    id: number,
    user : string,
    email : string,
}

const fetchUsers = async () : Promise<User[]> => {

    const data = await fetch(
     
            "https:jsonplaceholder.typicode.com/users"
        
    ).then(res => {
        return res.json()
    }).catch(err => {

      
            if(err instanceof Error) console.log(err.message)
 
    })
           
    return data
}

type fetchUsersReturnType = Awaited<ReturnType<typeof fetchUsers>>

fetchUsers().then(users => console.log(users))