//Generics - They provide us a placeholder for when we are unsure about the datatype of a variable

const echo = <T>(arg: T) : T => arg

// T = type variable placeholder

const isObj = <T>(arg : T): boolean => {
    return (typeof arg === 'object' && !Array.isArray(arg) && arg !==null)
}

console.log(isObj(true));
console.log(isObj('John'));
console.log(isObj([1,2,3]));
console.log(isObj({ name : 'John'}));
console.log(isObj(null));

const istrue = <T>(arg : T): { arg: T, is : boolean} =>
{
    if(Array.isArray(arg) && !arg.length){
        return { arg, is: false}
    }
    if(isObj(arg) && !Object.keys(arg as keyof T),length){
        return {arg, is:false}
    }
    return {arg, is : !!arg}
}

console.log(istrue(true));
console.log(istrue(false));
console.log(istrue(1));
console.log(istrue(0));
console.log(istrue(''));
console.log(istrue(null));
console.log(istrue(undefined));
console.log(istrue('John'));
console.log(istrue([]));
console.log(istrue([1,2,3]));
console.log(istrue({ }));
console.log(istrue({ name : 'John'}));
console.log(istrue(-0));
console.log(istrue(NaN));

// Redo with interface

interface BoolCheck<T> {

    value: T,
    is: boolean
}


const checkBoolValue = <T>(arg : T): BoolCheck<T> =>
{
    if(Array.isArray(arg) && !arg.length){
        return { value: arg, is: false}
    }
    if(isObj(arg) && !Object.keys(arg as keyof T),length){
        return {value : arg, is:false}
    }
    return {value : arg, is : !!arg}
}

interface hasId {

    id: number

}

const processUser = <T extends hasId>(user : T): T => {
    return user
}

console.log(processUser({id: 1, name : "John" }))
// console.log(processUser{ name : "John" })


const getUsersProperty = <T extends hasId, K extends keyof T>(users : T[], key : K): T[K][] => {
    return users.map((user) => user[key])
}

const usersArray = [

    {
        "id": 1,
        "name" : "Leane Graham",
        "userName" : "Bret",
        "email" : "leanegrahan@gmail.com",
        "address" : {
            "street" : "Kulas Light",
            "city": "Apt. 556",
            "geo" : {
                "lat" : "-37.37",
                "lang" : "81.14"
            }
        }
    },

    {
        "id": 2,
        "name" : "Lady Gaga",
        "userName" : "lala",
        "email" : "ladygaga@gmail.com",
        "address" : {
            "street" : "Kulas Light",
            "city": "Apt. 557",
            "geo" : {
                "lat" : "-38.37",
                "lang" : "82.14"
            }
        }
    },

    {
        "id": 3,
        "name" : "Sia liam",
        "userName" : "siaaaa",
        "email" : "sialiamn@gmail.com",
        "address" : {
            "street" : "Kulas Light",
            "city": "Apt. 558",
            "geo" : {
                "lat" : "-39.37",
                "lang" : "83.14"
            }
        }
    },

]


console.log(getUsersProperty(usersArray, "email"))

console.log(getUsersProperty(usersArray, "userName"))



//-----------------------------------------------------------

class StateObject<T> {

    private data : T

    constructor(value : T){
        this.data = value
    } 

    get state(): T{
        return this.data
    }

    set state(value : T){
        this.data = value
    }
}

const store = new StateObject("John")
console.log(store.state)
store.state = "Dave"
// store.state = 12


const myState = new StateObject<(string | number | boolean )[]>([15])
myState.state = (['Dave',42,true])

console.log(myState.state)
