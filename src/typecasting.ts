//Type casting or assertion

//aliases 
type strType = string;
type strOrNum = string | number;
type helloType = "Hello"

//convert to more or less specific
let strVar : strType = 'Hello'
let strNumVar = strVar as strOrNum //less specific
let helloVar = strVar as helloType //more specific

let d = <strType>"World"
let e = <string | number>"World"

const addOrConcat = (a: number, b:number, c:'add' | 'concat'): number | string => {
    if(c === 'add') return a + b
    return ' ' + a + b
}

let myVal: string = addOrConcat(2,2,"concat") as string

// Be careful 
let nextVal: number = addOrConcat(2,2,"concat") as number



// 10 as string

//unknown - forced casting! AVOID WHEN YOU CAN
(10 as unknown) as string

//The DOM
const img = document.querySelector('img')!
const myImg = document.getElementById('#img') as HTMLImageElement
const myNextImg = <HTMLImageElement>document.getElementById('#img')

img.src
myImg.src