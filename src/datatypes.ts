// let username: string = "Maitreyi";
// let meaningOflife: number;
// let isLoading: boolean;
// let album: string | number; //Union Type

// username = "Sheetal"
// meaningOflife = 0
// isLoading = true
// // album = 1984
// album = "Lana Del Rey"


// const sum = (a: number , b:string) => {
//     return a + b //Returns String.
// }

// let postId : string | number;
// let isActive: number | boolean;


// let re: RegExp = /\w+/g



// let stringArr = ['one','two','three']

// let guitars = ["strat",'les Paul', 5190]

// let mixedData = ['EVH', 1984, true]

// // stringArr[0] = 45; NOT ALLOWED
// stringArr[0] = 'four';

// guitars[0] = 1984

// // mixedData[0] = null;

// let test = [];
// //annotation as string
// let bands: string[] = [];
// bands.push('Van Halen')


// let myTuple: [string,number,boolean]  = ["sring",5,false]

// let mixed = ["sring", 5, false]

// myTuple[1] = 45


// //Objects
// let myObj: object 

// myObj = []
// console.log(typeof myObj)
// myObj = bands;

// myObj = {}


// // Datatypes get assignmend!
// const exampleObj = {
//     prop1 : "Dave",
//     prop2: true,
// }


// //Type making

// // type Guitarist = {
// //     name : string,
// //     active?: boolean,
// //     albums :(string | number)[]
// // }

// //Interface making SAME AS TYPE

// interface Guitarist {
//     name?: string,
//     active: boolean,
//     albums :(string | number)[]
// }


// let evh: Guitarist ={
//     name: "Naina",
//     active: false,
//     albums : [2990,1992,1993]
// }

// let jp: Guitarist ={
//     // name: "Naira",
//     active: true,
//     albums : ['I','II','III']
// }

// evh = jp
// // evh.years;

// const greetGuitarist = (guitarist: Guitarist) => {
//     if(guitarist.name){
//         return `Hello ${guitarist.name.toUpperCase()}!`
//     }
//     else{
//         return "Hello!"
//     }
// }

// // console.log(greetGuitarist(jp))

// //ENUM

// enum Grade {
//     A,
//     B,
//     C,
//     D,
// }

// console.log(Grade.B)