// Index Signature - Dynamically adding key value pairs into objects.


// interface TransactionObj {

//     Pizza : number,
//     Books : number,
//     Job : number
// }

interface TransactionObj {

    //Keys cannot be a boolean!
    //Can be dynamically added
    readonly [key : string] : number,

    //Must have these properties 
    Pizza : number,
    Books : number,
    Job : number
   
}

const todaysTransactions : TransactionObj = {
   
    Pizza : -10,
    Books : -5,
    Job : 50

}

console.log(todaysTransactions.Pizza);
console.log(todaysTransactions['Pizza']);

let prop : string = 'Pizza'
console.log(todaysTransactions[prop])

const todaysNet = (transactions : TransactionObj): number => {
    let total = 0
    for(const transaction in transactions){
        total += transactions[transaction]
        // console.log(transactions,transaction)
    }
    return total;
}

console.log(todaysNet(todaysTransactions))

// todaysTransactions.Pizza = 40;

console.log(todaysTransactions['Dave'])


// -----------------------------------------------------------------------------------------------------------------------------------------------


interface Student {

    // [key : string] : string | number | number[] | undefined

    studentName: string,
    GPA : number,
    classes?: number[]

}

const student : Student = {

    studentName: "Doug",
    GPA : 3.5,
    classes: [10,20]
}

// console.log(student.test)

for(const key in student){   
    // console.log(student[key]);
    console.log(`${key} : ${student[key as keyof Student]}`);
}

Object.keys(student).map((key) => console.log(student[key as keyof typeof student]))

const logStudentKey = (student : Student, key: keyof Student) : void => {

    console.log(`Student ${key} : ${student[key]}`);
}

logStudentKey(student,'studentName')


// -----------------------------------------------------------------------------------------------------------------------------------------------

// interface Incomes {

//         [key : string] : number
//         // Pizza : string

// }

type streams = "salary" | "bonus" | "sidehustle"

type incomes = Record<streams, number | string>

const monthlyIncomes : incomes = {

    salary: 500,
    bonus: 100,
    sidehustle : 250
}

for(const revenue in monthlyIncomes){
    console.log(monthlyIncomes[revenue as keyof incomes])
}