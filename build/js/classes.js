"use strict";
// CLASSES
// class Coder {
//     name:string
//     music:string
//     age: number
//     lang: string
//     constructor(
//         name:string,
//         music:string,
//         age: number,
//         lang: string
//         ){
//         this.name = name
//         this.music = music
//         this.age = age
//         this.lang = lang
//     }
// }
class Coder {
    constructor(name, music, age, lang = "Typescript") {
        this.name = name;
        this.music = music;
        this.age = age;
        this.lang = lang;
        this.name = name;
        this.music = music;
        this.age = age;
        this.lang = lang;
    }
    getAge() {
        return `Hello I'm ${this.age}`;
    }
}
// const dave = new Coder('Dave','Rock',42,'Typescript')
const dave = new Coder('Dave', 'Rock', 42);
console.log(dave.getAge());
// console.log(dave.age)
// console.log(dave.lang)
class WebDev extends Coder {
    constructor(computer, name, music, age) {
        super(name, music, age);
        this.computer = computer;
        this.computer = computer;
    }
    getLang() {
        return `I code in ${this.lang}`;
    }
}
const sara = new WebDev("Mac", "sara", "lofi", 25);
console.log(sara.getLang());
console.log(sara.getAge());
class Guitarist {
    constructor(userName, instrument) {
        this.userName = userName;
        this.instrument = instrument;
    }
    play(action) {
        return `${this.userName} ${action} the ${this.instrument}`;
    }
}
const page = new Guitarist("Jimmy", 'guitar');
console.log(page.play('strums'));
class Peeps {
    static getCount() {
        return Peeps.count;
    }
    constructor(name) {
        this.name = name;
        this.name = name;
        this.id = ++Peeps.count;
    }
}
Peeps.count = 0;
const john = new Peeps("John");
const steve = new Peeps("Steve");
const amy = new Peeps("Amy");
console.log(Peeps.count); //3
console.log(steve.id);
console.log(john.id);
console.log(amy.id);
// GETTERS AND SETTERS
class Bands {
    constructor() {
        this.dataState = [];
    }
    get data() {
        return this.dataState;
    }
    set data(value) {
        if (Array.isArray(value) && value.every((element) => typeof element === "string")) {
            this.dataState = value;
            return;
        }
        else
            throw new Error("Parameter is not an array of strings");
    }
}
const myBand = new Bands();
myBand.data = ['Neil Young', 'Led Zep'];
console.log(myBand.data);
myBand.data = [...myBand.data, "ZZ Top"];
console.log(myBand.data);
myBand.data = ['Van Halen'];
