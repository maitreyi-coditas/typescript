"use strict";
//Type casting or assertion
//convert to more or less specific
let strVar = 'Hello';
let strNumVar = strVar; //less specific
let helloVar = strVar; //more specific
let d = "World";
let e = "World";
const addOrConcat = (a, b, c) => {
    if (c === 'add')
        return a + b;
    return ' ' + a + b;
};
let myVal = addOrConcat(2, 2, "concat");
// Be careful 
let nextVal = addOrConcat(2, 2, "concat");
// 10 as string
//unknown - forced casting! AVOID WHEN YOU CAN
10;
//The DOM
const img = document.querySelector('img');
const myImg = document.getElementById('#img');
const myNextImg = document.getElementById('#img');
img.src;
myImg.src;
